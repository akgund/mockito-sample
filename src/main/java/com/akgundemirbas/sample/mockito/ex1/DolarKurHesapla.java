package com.akgundemirbas.sample.mockito.ex1;

public class DolarKurHesapla {

    public double hesapla(double tl) {
        KurServisi kurServisi = new KurServisi();

        final double dolarKuru = kurServisi.dolarKuru();

        return tl * dolarKuru;
    }
}
