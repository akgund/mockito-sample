package com.akgundemirbas.sample.mockito.ex1;

import java.util.Random;

public class KurServisi {

    public double dolarKuru() {
        final Random random = new Random();

        return random.nextDouble();
    }
}
