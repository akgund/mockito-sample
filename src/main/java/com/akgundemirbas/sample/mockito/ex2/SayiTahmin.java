package com.akgundemirbas.sample.mockito.ex2;

import java.util.Random;

public class SayiTahmin {

    public boolean tahminEt(int sayi) {
        final int rastgeleSayi = yeniSayi();

        if (rastgeleSayi == sayi) {
            return true;
        }

        return false;
    }

    private int yeniSayi() {
        final Random random = new Random();

        return random.nextInt(10) + 1;
    }
}
