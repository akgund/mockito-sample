package com.akgundemirbas.sample.mockito.ex3;

public class MusteriMemnuniyetAnketi {
    private MusteriVeritabani musteriVeritabani = new MusteriVeritabani();

    public void gonder(int puan) {
        if (puan < 2) {
            musteriVeritabani.kaydet("begenmedi");
            return;
        }

        musteriVeritabani.kaydet("begendi");
    }
}
