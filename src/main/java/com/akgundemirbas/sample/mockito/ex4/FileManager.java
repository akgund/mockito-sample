package com.akgundemirbas.sample.mockito.ex4;

public class FileManager {

    public void save(String filePath) {
        if (!FileUtils.isExists(filePath)) {
            System.out.println("Could not save " + filePath);
            return;
        }

        System.out.println("Saved " + filePath);
    }
}
