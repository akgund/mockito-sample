package com.akgundemirbas.sample.mockito.ex4;

import java.io.File;

public class FileUtils {

    public static boolean isExists(String filePath) {
        if (filePath == null) {
            return false;
        }

        return new File(filePath).exists();
    }
}
